import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {

  //instance variables (object lifetime)
  result: string;
  computer: string;
  playOptions: string[];

  ngOnInit(): void {

    this.playOptions = ["Rock", "Paper", "Scissors"];
  }

  constructor() { }

  check(event) {

    console.log("button " + event.target.textContent +  "clicked");

    //local variables, only visible within the block (function)
    const rand = Math.floor(Math.random() * this.playOptions.length);
    const computersChoice: string = this.playOptions[rand];
    //assign local data into an instance variable to access data binding
    //not a practical choice here, but doable if necessary
    this.computer = computersChoice;
    //trimming will remove spaces around non-inlined (in markup) button text content
    this.getResult(event.target.textContent.trim(), computersChoice);
  }

  //arguments will act as local variables inside the function block
  getResult(pC: string, cC: string) {

    console.log(pC + ", " + cC);

    if (cC === pC) {
      console.log("draw");
      this.result = "It's a draw!";
    }
    else if (cC === this.playOptions[0] && pC === this.playOptions[1] ||
      cC === this.playOptions[1] && pC === this.playOptions[2] ||
      cC === this.playOptions[2] && pC === this.playOptions[0]
    ) {
      console.log("player wins");
      this.result = "You win!";
    }
    else {
      console.log("computer wins");
      this.result = "I win!";
    }
  }

}
